@extends ('layouts.layout')

@section('content')

{{--first form--}}
    <section class="ftco-consult bg-light">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-2 text-lg-right">
                    <h3 class="mb-4 mb-lg-0">My Free Consultation</h3>
                </div>
                <div class="col-lg-10">
                    <form  method="post" action="#" class="consult-form needs-validation" id="firstform">
                        @csrf
                        <div class="d-lg-flex align-items-md-end">
                            <div class="form-group mb-3 mb-lg-0 mr-4">
                                <label for="#">Name</label>
                                <input type="text" class="name form-control" placeholder="Name *" name="name" id="name">
                                <div class="error-box"></div>
                            </div>
                            <div class="form-group mb-3 mb-lg-0 mr-4">
                                <label for="#">Email Address</label>
                                <input type="text" class="email form-control" placeholder="Email Address *" name="email" id="email">
                                <div class="error-box"></div>
                            </div>
                            <div class="form-group mb-3 mb-lg-0 mr-4">
                                <label for="cat">Categories(optional)</label>
                                <div class="form-field">
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="type" id="type" class="form-control">
                                            <option value="Family Law">Family Law</option>
                                            <option value="Labor Law">Labor Law</option>
                                            <option value="Business Ligitation">Business Ligitation</option>
                                            <option value="Employment Law">Employment Law</option>
                                            <option value="Criminal Law">Criminal Law</option>
                                            <option value="No Category">No Category</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-3 mb-lg-0 mr-4">
                                <label for="#">Message</label>
                                <textarea name="message" id="text" cols="30" rows="3" class=" text form-control d-flex align-items-center" placeholder="Message"></textarea>
                                <div class="error-box"></div>
                            </div>

                            <input type="hidden" name="message_type" value="consultation">

                            <div class="form-group">
                                <input type="submit" id="firstsend" value="Send Message" class="btn btn-primary py-3 px-4">
                            </div>
                        </div>
                    </form>
                    <div id="my_message" style="height: 30px;"></div>
                </div>
            </div>
        </div>
    </section>

{{--second form--}}
    <section class="ftco-section ftc-no-pb ftc-no-pt bg-light">
        <div class="container">
            <div class="row align-items-md-center">
                <div class="col-md-5 pt-5">
                    <form method="post" action="#" id="second_form" class="consult-form py-5">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="#">Name</label>
                                    <input type="text" name="name" id="name" class="name form-control" placeholder="Name">
                                    <div class="error-box"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="#">Email</label>
                                    <input type="text" id="email" name="email" class="email form-control" placeholder="Email">
                                    <div class="error-box"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="#">Date</label>
                                    <div class="form-field">
                                        <input type="text" name="date" class="form-control checkin_date" placeholder="Date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="#">Categories(optional)</label>
                                    <div class="form-field">
                                        <div class="select-wrap">
                                            <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                            <select name="type" id="" class="form-control">
                                                <option value="Family Law">Family Law</option>
                                                <option value="Labor Law">Labor Law</option>
                                                <option value="Business Ligitation">Business Ligitation</option>
                                                <option value="Employment Law">Employment Law</option>
                                                <option value="Criminal Law">Criminal Law</option>
                                                <option value="No Category">No Category</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group mb-4">
                                    <label for="#">Message</label>
                                    <textarea name="message" id="text" cols="30" rows="7" class="text form-control form-control-2 d-flex align-items-center" placeholder="Message"></textarea>
                                </div>
                            </div>

                            <input type="hidden" name="message_type" value="appointment">

                            <div class="col-md-12">
                                <div class="form-group mb-4">
                                    <input type="submit" value="Make an Appointment" id="app_btn" class="btn btn-primary py-3 px-4">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-7 wrap-about pb-5 ftco-animate">
                    <div class="heading-section mb-md-5 pl-md-5 mt-md-5 pt-3">
                        <div class="pl-md-3">
                            <span class="subheading">Appointment</span>
                            <h2 class="mb-4">Make An Appointment</h2>
                        </div>
                    </div>
                    <div class="pl-md-3">
                        <p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                        <p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts')
    <script>

        $(document).ready(function(){

            $('#name, #email, #text').unbind().blur( function(){

                let id = $(this).attr('id');
                let val = $(this).val();

                switch(id) {

                    case 'name':
                        if(val !== '') {
                            $(this).addClass('not_error');
                            $(this).css('border-color','green')
                        }
                        else {
                            $(this).removeClass('not_error');
                            $(this).css('border-color','red')
                        }
                        break;
                    case 'email':
                        if(val !== '') {
                            $(this).addClass('not_error');
                            $(this).css('border-color','green')
                        }
                        else {
                            $(this).removeClass('not_error');
                            $(this).css('border-color','red')
                        }
                        break;
                    case 'text':
                        if(val !== '') {
                            $(this).addClass('not_error');
                            $(this).css('border-color','green')
                        }
                        else {
                            $(this).removeClass('not_error');
                            $(this).css('border-color','red')
                        }
                        break;
                }

            });


            $('#firstform').submit(function(){

                e.preventDefault();

                if($('.not_error').length == 3) {
                    $.post('{{ route('consultation') }}', $("#firstform").serialize(), function (msg) {
                        $('#my_message').html(msg);
                    });
                }

                return false;
            });

            $('#second_form').submit(function(){
                $.post('{{ route('consultation') }}', $("#second_form").serialize());

                return false;
            });
        });

    </script>
    @endpush