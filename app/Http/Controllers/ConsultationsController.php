<?php

namespace App\Http\Controllers;

use App\Consultation;
use Illuminate\Http\Request;

class ConsultationsController extends Controller
{
    public function index()
    {
        return view ('welcome');
    }

    public function feedback(Request $request)
    {
        Consultation::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'date' => $request->get('date'),
            'type' => $request->get('type'),
            'message' => $request->get('message'),
            'message_type' => $request->get('message_type'),
        ]);


        echo '<b style="color: green">Successful!</b>';

    }
}
